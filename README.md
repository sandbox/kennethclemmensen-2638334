INTRODUCTION
------------
 
This module provides an option to download a file
and counts the number of downloads.

STEP-BY-STEP GUIDE
------------------
This guide describes how to use the File Download formatter.
The formatter can only be used on file fields.

1. Install the module.
2. Enable the module.
3. Go to Admin > Structure > Content Types, choose a content type
   and click on the Manage Display button.
4. Find a file field and choose the File Download formatter for the field
   in the Format column.
5. Save the content type.
6. Go to Admin > People > Permissions to give the users permission
   to download files.
